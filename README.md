# chrome-extension-react-boilerplate
For start coding Chrome Extension quickly using Typescript and React, and build with Webpack.

## Installation
```sh
# Download the project
git clone 

# Install dependencies
yarn install
```

## Development
1. `yarn start` to start build file with watch mode and a server to hot reload chrome extension.
2. open chrome navigate `chrome://extensions` then switch on `Developer mode`,finally click *Load unpacked extension* then load `dist` directory.

## build
1. `yarn build`